<?php
/***************************************************************************
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Francois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'tableau_de_bord' => 'Tableau de bord',
	'titre_page_config' => 'Configuration du plugin',
	'erreur_probleme_technique' => 'Problème technique',
	'erreur_probleme_technique2' => 'Problème technique',
	'ok_mdp_modifie' => 'Votre mot de passe a bien été modifié',
	'configuration_generale' => 'Configuration générale',
	'url_logiciel' => 'URL de l\'API simplasso',
	'acces_restreints' => 'id_auteur_adherent ',
	'compte_generique' => 'Compte générique',
	'id_auteur_adherent' => 'Auteur adhérent',
	'acces_donnees_spip' => 'Accès aux données de SPIP',
	'acces_donnees_spip_ouinon' => 'Autoriser l\'accès aux données de SPIP',
	'mailsubscribers_ouinon' => 'Autoriser  l\'accès aux données de mailsubscribers',
	'acces_restreints_ouinon' => 'Autoriser l\'accès aux données de mailsubscribers',
	'restriction_ip' => 'Adresse IP acceptées',
	'adresse' => 'Adresse',
	'ville' => 'Ville',
	'code_postal' => 'Code postal',
	'pays' => 'Pays',
	'mobile' => 'Mobile',
	'telephone' => 'Téléphone',
	'email' => 'Email',
	'simplasso' => 'Simplasso',
	'recevoir_mail_activation' => 'Un courriel vient de vous être envoyé, il contient les informations pour poursuivre la procédure.',
	'ok_connection_possible'=>'Vous pouvez désormais accéder à votre compte adhérent en utilisant votre login et le mot de passe que vous venez de saisir.'

);




