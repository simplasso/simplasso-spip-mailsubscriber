<?php

include(__DIR__.'/../vendor/autoload.php');



use JsonRPC\Server;


/**
 * Plugin simplasso
 **
 * Action simplasso _serveur
 */

if (!defined("_ECRIRE_INC_VERSION")) {
    return;
}


function action_simplasso_serveur_dist()
{

    include_spip('inc/config');

    $server = new Server();
    spip_log(lire_config('simplmailsub/restriction_ip', ['127.0.0.1']),_LOG_ALERTE_ROUGE);
    $server->allowHosts(explode(',', lire_config('simplmailsub/restriction_ip', array('127.0.0.1'))));
    $server->authentication(["api_user_simplmailsub"=>lire_config('simplmailsub/mot_de_passe_complexe', 'durdur')]);
    if (!empty(lire_config('simplmailsub/mot_de_passe_complexe'))) {


    $server->getProcedureHandler()
        ->withCallback('newsletter_liste', function () {
            $tab_newsletters = sql_allfetsel('id_mailsubscribinglist as id_liste, titre, descriptif',
                'spip_mailsubscribinglists');
            return $tab_newsletters;
        })
        ->withCallback('newsletter_statut', function ($email) {
            $tab_statut = array();
            $tab_newsletters = sql_allfetsel('id_mailsubscribinglist', 'spip_mailsubscribinglists');
            $id_mailsubscriber = sql_getfetsel('id_mailsubscriber', 'spip_mailsubscribers',
                'email=' . sql_quote($email));
            foreach ($tab_newsletters as $n) {
                $tab_statut[$n['id_mailsubscribinglist'] . ''] = sql_countsel('spip_mailsubscriptions',
                        'id_mailsubscriber = ' . $id_mailsubscriber . ' and id_mailsubscribinglist=' . $n['id_mailsubscribinglist']) > 0;
            }
            return $tab_statut;
        })
        ->withCallback('newsletter_inscrits', function () {
            $tab_inscrit = array();
            $tab_newsletters = sql_allfetsel('id_mailsubscribinglist as id', 'spip_mailsubscribinglists');

            foreach ($tab_newsletters as $n) {
                $tab = sql_allfetsel('email', 'spip_mailsubscribers i,spip_mailsubscriptions m',
                    'm.id_mailsubscriber  = i.id_mailsubscriber and id_mailsubscribinglist=' . $n['id']);
                foreach ($tab as &$t) {
                    $t = $t['email'];
                }
                $tab_inscrit[$n['id']] = $tab;
            }
            return $tab_inscrit;
        })
        ->withCallback('newsletter_inscription', function ($id_liste, $email, $nom) {
            $ok = false;
            $message = '';
            $id_mailsubscriber = sql_getfetsel('id_mailsubscriber', 'spip_mailsubscribers',
                'email=' . sql_quote($email));
            if (!$id_mailsubscriber) {
                $tab_info = array(
                    'email' => $email,
                    'nom' => $nom,
                    'statut' => 'valide'
                );
                $id_mailsubscriber = sql_insertq('spip_mailsubscribers', $tab_info);
            }
            if ($id_mailsubscriber) {
                $nb = sql_countsel('spip_mailsubscriptions',
                        'id_mailsubscriber = ' . $id_mailsubscriber . ' and id_mailsubscribinglist=' . $id_liste) > 0;
                if ($nb==0) {
                    sql_insertq('spip_mailsubscriptions', array(
                        'id_segment' => 0,
                        'statut' => 'valide',
                        'actualise_segments' => 0,
                        'id_mailsubscriber' => $id_mailsubscriber,
                        'id_mailsubscribinglist' => $id_liste
                    ));
                    sql_updateq('spip_mailsubscribers',['statut'=>'valide'],'id_mailsubscriber = ' . $id_mailsubscriber);
                }
                $ok = true;
            }
            return [$ok,$message];
        })
        ->withCallback('newsletter_desinscription', function ($id_liste, $email) {
            $ok = false;
            $message='';
            $id_mailsubscriber = sql_getfetsel('id_mailsubscriber', 'spip_mailsubscribers',
                'email=' . sql_quote($email));
            if ($id_mailsubscriber) {
                $nb = (int) sql_countsel('spip_mailsubscriptions',
                        'id_mailsubscriber = ' . $id_mailsubscriber . ' and id_mailsubscribinglist=' . $id_liste);
                if ($nb > 0) {
                    sql_delete('spip_mailsubscriptions',
                        'id_mailsubscriber=' . $id_mailsubscriber . ' and id_mailsubscribinglist = ' . $id_liste);
                    $ok = true;
                    //Traitement pour mettre le mailsubscipter à la poubelle si plus d'inscription
                    $nb = (int) sql_countsel('spip_mailsubscriptions','id_mailsubscriber = ' . $id_mailsubscriber );
                    if ($nb === 0) {
                        sql_updateq('spip_mailsubscribers',['statut'=> 'poubelle'],'id_mailsubscriber = ' . $id_mailsubscriber);
                    }

                }
            }
            else{
                $message = 'Email introuvable';
            }
            return [$ok,$message];
        });

        echo $server->execute();

   }

    exit();

}

